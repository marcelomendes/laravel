<?php namespace App\Repositories\Eloquent;

use App\Produto;
use App\Repositories\AbstractRepository;
use App\Repositories\Contracts\ProdutoRepositoryInterface;

class ProdutoRepository extends AbstractRepository implements ProdutoRepositoryInterface
{
    /**
    * @param Produto $model
    */

    public function __construct(Produto $model)
    {
        /** @var Produto $model */
        $this->model = $model;
    }
}
